package ru.dburyakov.web_showcase.http.dto;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.time.LocalDateTime;

public class DTOXmlMarshallingTest {

    private BookDTO testData = new BookDTO()
            .withId(1L)
            .withTitle("test")
            .withReleaseDate(LocalDateTime.now())
            .withAuthor(new Reference<>(2L));

    @Test
    public void testBookDtoMarshalling() throws JAXBException {
        JAXBContext jaxb = JAXBContext.newInstance(BookDTO.class);
        Marshaller marshaller = jaxb.createMarshaller();
        StringWriter xml = new StringWriter();

        marshaller.marshal(testData, xml);

        assertNotEquals(xml.toString().trim(), "");

    }

}
