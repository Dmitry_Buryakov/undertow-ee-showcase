package ru.dburyakov.web_showcase.context;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.inject.matcher.Matchers;
import com.google.inject.servlet.ServletModule;
import ru.dburyakov.web_showcase.http.ObjectMapperContextResolver;
import ru.dburyakov.web_showcase.http.endpoint.ShowcaseEndpoint;
import ru.dburyakov.web_showcase.http.endpoint.StreamBodyWriter;
import ru.dburyakov.web_showcase.service.impl.JpaBookService;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.transaction.Transactional;
import java.text.SimpleDateFormat;

public class ProductionModule extends ServletModule {

    @Override
    protected void configureServlets() {

        bind(EntityManagerHolder.class);
        bind(EntityManagerFactory.class).toInstance(entityManagerFactory("jpa_showcase"));
        bind(EntityManager.class).toProvider(EntityManagerHolder.class);

        bind(DataPopulator.class).asEagerSingleton();
        bind(JpaBookService.class).asEagerSingleton();
        bind(ShowcaseEndpoint.class);
        bind(ObjectMapper.class).toInstance(objectMapper());
        bind(ObjectMapperContextResolver.class);
        bind(StreamBodyWriter.class);

        TransactionalInterceptor transactionInterceptor = new TransactionalInterceptor();

        requestInjection(transactionInterceptor);
        bindInterceptor(Matchers.annotatedWith(Transactional.class), Matchers.any(), transactionInterceptor);
        bindInterceptor(Matchers.any(), Matchers.annotatedWith(Transactional.class), transactionInterceptor);
    }

    private EntityManagerFactory entityManagerFactory(String puName) {
        return Persistence.createEntityManagerFactory(puName);
    }

    private ObjectMapper objectMapper() {
        return new ObjectMapper()
                .setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"))
                .registerModule(new JavaTimeModule());
    }

    private class TransactionalInterceptor implements org.aopalliance.intercept.MethodInterceptor {

        @Inject
        private EntityManagerHolder emFactoryHolder;

        public Object invoke(org.aopalliance.intercept.MethodInvocation invocation) {
            return emFactoryHolder.transaction(invocation::proceed);
        }
    }
}
