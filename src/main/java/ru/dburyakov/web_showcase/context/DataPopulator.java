package ru.dburyakov.web_showcase.context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.dburyakov.web_showcase.entity.Author;
import ru.dburyakov.web_showcase.entity.Book;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.time.LocalDateTime;

public class DataPopulator {

    private Logger log = LoggerFactory.getLogger(getClass());

    @Inject
    private EntityManager em;

    @Transactional
    public void populateData() {
        Author author1 = new Author();
        author1.setFirstName("Лев");
        author1.setMiddleName("Николаевич");
        author1.setLastName("Толстой");
        author1.setBirthDate(LocalDateTime.of(1828, 8, 28, 0, 0));
        author1.setDeathDate(LocalDateTime.of(1910, 11, 7, 0, 0));

        em.persist(author1);

        Book book1 = new Book();
        book1.setTitle("Война и мир");
        book1.setAuthor(author1);
        book1.setReleaseDate(LocalDateTime.of(1867, 1, 1, 0, 0));
        book1.setAuthor(author1);
        em.persist(book1);

        Book book2 = new Book();
        book2.setTitle("Анна Каренина");
        book2.setAuthor(author1);
        book2.setReleaseDate(LocalDateTime.of(1873, 1, 1, 0, 0));
        book2.setAuthor(author1);
        em.persist(book2);

        log.info(" ---> Data populated {}", em);
    }


}
