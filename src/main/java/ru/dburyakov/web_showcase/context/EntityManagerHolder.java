package ru.dburyakov.web_showcase.context;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import java.util.Optional;

@Singleton
public class EntityManagerHolder implements Provider<EntityManager> {

    @Inject
    private EntityManagerFactory emFactory;

    private final ThreadLocal<EntityManager> entityManager = new ThreadLocal<>();


    @Override
    public synchronized EntityManager get() {
        return Optional.ofNullable(entityManager.get()).orElseGet(() -> {
            EntityManager newEm = emFactory.createEntityManager();
            System.out.println("----------> create em for "+Thread.currentThread() + " -> " + newEm + " on provider "+ this);
            entityManager.set(newEm);
            return newEm;
        });
    }

    @FunctionalInterface
    public interface TransactionCallable<T> {
        T apply() throws Throwable;
    }

    public <T> T transaction(TransactionCallable<T> txAction) {
        EntityManager threadEm = null;
        EntityTransaction threadTx = null;
        try{
            threadEm = this.get();
            threadTx = threadEm.getTransaction();
            synchronized (entityManager) {
                if (!threadTx.isActive()) {
                    System.out.println("-----------> begin tx for " + threadEm);
                    threadEm.getTransaction().begin();
                }
            }
            return txAction.apply();

        } catch (Throwable e) {
            e.printStackTrace();
            if (threadEm != null && threadTx != null) {
                synchronized (entityManager) {
                    threadTx.setRollbackOnly();
                    if (threadTx.isActive()) {
                        System.out.println("-----------> rollback tx for " + threadEm);
                        threadTx.rollback();
                    } else {
                        System.out.println("-----------> not active rollback tx for " + threadEm);
                    }
                }
            }
            return null;
        } finally {
            if (threadTx != null && !threadTx.getRollbackOnly()){
                synchronized (entityManager) {
                    if (threadTx.isActive()) {
                        System.out.println("-----------> commit tx for " + threadEm);
                        threadTx.commit();
                    } else {
                        System.out.println("-----------> not active commit tx for " + threadEm);
                    }
                }
            }
            if (threadEm != null) {
                System.out.println("-----------> close em for " + threadEm);
                threadEm.close();
                entityManager.remove();
            }
        }
    }
}
