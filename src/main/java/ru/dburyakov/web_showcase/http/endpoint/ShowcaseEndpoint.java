package ru.dburyakov.web_showcase.http.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.dburyakov.web_showcase.http.dto.BookDTO;
import ru.dburyakov.web_showcase.service.impl.JpaBookService;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.stream.Stream;

@Path("/showcase")
public class ShowcaseEndpoint {

    @Inject
    private JpaBookService jpaBookService;
    @Inject
    private ObjectMapper objectMapper;


    @GET @Path("/books")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Stream<BookDTO> getAllBooks() {
        return jpaBookService.allBooks();
    }

    @GET @Path("/book/{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public BookDTO getBookById(@PathParam("id") Long id) {
        return jpaBookService.findBook(id).orElse(null);
    }


}
