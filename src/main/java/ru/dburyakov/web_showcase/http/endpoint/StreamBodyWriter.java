package ru.dburyakov.web_showcase.http.endpoint;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.reflect.TypeToken;
import org.jboss.resteasy.plugins.providers.jaxb.JAXBContextFinder;
import ru.dburyakov.web_showcase.Launcher;
import ru.dburyakov.web_showcase.http.dto.Reference;

import javax.inject.Inject;
import javax.ws.rs.NotAcceptableException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.ext.Providers;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.stream.Stream;

@Provider
public class StreamBodyWriter<T> implements MessageBodyWriter<Stream<T>> {

    @Inject
    private ObjectMapper objectMapper;
    @Context
    protected Providers providers;

    @Override
    public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        System.out.println("----------> check against "+type + " -> " + Stream.class.isAssignableFrom(type));
        return Stream.class.isAssignableFrom(type);
    }

    @Override
    public void writeTo(Stream<T> stream, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, Object> httpHeaders, OutputStream entityStream) throws IOException, WebApplicationException {
        if (mediaType.equals(MediaType.APPLICATION_JSON_TYPE)) {
            JsonGenerator json = objectMapper.getFactory().createGenerator(entityStream, JsonEncoding.UTF8);
            json.writeStartArray();
            stream.forEach(book -> {
                try {
                    json.writeObject(book);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            json.writeEndArray();
            json.close();
        } else if (mediaType.isCompatible(MediaType.APPLICATION_XML_TYPE)) {
            try {
                Class<T> streamElType = (Class<T>) ((ParameterizedType) genericType).getActualTypeArguments()[0];
                System.out.println("-------------------> write xml " + streamElType);
                JAXBContext jaxbContext = providers.getContextResolver(JAXBContextFinder.class, mediaType)
                        .getContext(null)
                        .findCachedContext(streamElType, mediaType, annotations);

                //JAXBContext jaxbContext = JAXBContext.newInstance();
                XMLStreamWriter xmlOut = XMLOutputFactory.newFactory().createXMLStreamWriter(entityStream);
                Marshaller marshaller = jaxbContext.createMarshaller();
                stream.forEach(book -> {
                    try {
                        marshaller.marshal(book, xmlOut);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
                xmlOut.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("-------------------> reject " + mediaType);
            throw new NotAcceptableException();
        }
    }
}
