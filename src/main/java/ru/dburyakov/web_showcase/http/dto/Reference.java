package ru.dburyakov.web_showcase.http.dto;

import javax.xml.bind.annotation.*;
import java.io.Serializable;

@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
public class Reference<T> implements Serializable {

    @XmlElement
    private T id;

    public Reference(T id) {
        this.id = id;
    }

    public T getId() {
        return id;
    }
}
