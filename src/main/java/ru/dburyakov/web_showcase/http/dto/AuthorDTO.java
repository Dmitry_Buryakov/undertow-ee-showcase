package ru.dburyakov.web_showcase.http.dto;

import ru.dburyakov.web_showcase.entity.Author;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

@XmlType
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class AuthorDTO implements Serializable {

    @XmlElement
    private Long id;

    @XmlElement
    private String firstName;
    @XmlElement
    private String middleName;
    @XmlElement
    private String lastName;
    @XmlElement
    private LocalDateTime birthDate;
    @XmlElement
    private LocalDateTime deathDate;

    @XmlElement
    //@XmlElementWrapper(name = "books")
    private Collection<Reference<Long>> books = new ArrayList<>();

    public static AuthorDTO of(Author in) {
        return new AuthorDTO()
                .withId(in.getId())
                .withFirstName(in.getFirstName())
                .withMiddleName(in.getMiddleName())
                .withLastName(in.getLastName())
                .withBirthDate(in.getBirthDate())
                .withDeathDate(in.getDeathDate().orElse(null))
                .withBooks(
                    in.getBooks().stream().map(book -> new Reference<>(book.getId())).collect(Collectors.toList())
                );

    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setBirthDate(LocalDateTime birthDate) {
        this.birthDate = birthDate;
    }

    public void setDeathDate(LocalDateTime deathDate) {
        this.deathDate = deathDate;
    }

    public void setBooks(Collection<Reference<Long>> books) {
        this.books = books;
    }

    public Collection<Reference<Long>> getBooks() {
        return books;
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public LocalDateTime getBirthDate() {
        return birthDate;
    }

    public LocalDateTime getDeathDate() {
        return deathDate;
    }

    public AuthorDTO withId(Long id) {
        this.id = id;
        return this;
    }

    public AuthorDTO withFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public AuthorDTO withMiddleName(String middleName) {
        this.middleName = middleName;
        return this;
    }

    public AuthorDTO withLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public AuthorDTO withBirthDate(LocalDateTime birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public AuthorDTO withDeathDate(LocalDateTime deathDate) {
        this.deathDate = deathDate;
        return this;
    }

    public AuthorDTO withBooks(Collection<Reference<Long>> books) {
        this.books = books;
        return this;
    }

}
