package ru.dburyakov.web_showcase.http.dto;

import ru.dburyakov.web_showcase.entity.Book;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@XmlType
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BookDTO implements Serializable {

    @XmlElement
    private Long id;

    @XmlElement
    private Reference<Long> author;
    @XmlElement
    private String title;
    @XmlElement
    private LocalDateTime releaseDate;

    public static BookDTO of(Book in) {
        return new BookDTO()
                .withId(in.getId())
                .withTitle(in.getTitle())
                .withReleaseDate(in.getReleaseDate())
                .withAuthor(new Reference<>(in.getAuthor().getId()));
    }

    public Long getId() {
        return id;
    }

    public Reference<Long> getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public LocalDateTime getReleaseDate() {
        return releaseDate;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setAuthor(Reference<Long> author) {
        this.author = author;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setReleaseDate(LocalDateTime releaseDate) {
        this.releaseDate = releaseDate;
    }

    public BookDTO withId(Long id) {
        this.id = id;
        return this;
    }

    public BookDTO withAuthor(Reference<Long> author) {
        this.author = author;
        return this;
    }

    public BookDTO withTitle(String title) {
        this.title = title;
        return this;
    }

    public BookDTO withReleaseDate(LocalDateTime releaseDate) {
        this.releaseDate = releaseDate;
        return this;
    }

}
