@XmlJavaTypeAdapters({
        @XmlJavaTypeAdapter(type = LocalDateTime.class, value = LocalDateTimeAdapter.class),
        @XmlJavaTypeAdapter(type = Long.class, value = LongAdapter.class),
})
package ru.dburyakov.web_showcase.http.dto;

import ru.dburyakov.web_showcase.xml.LocalDateTimeAdapter;
import ru.dburyakov.web_showcase.xml.LongAdapter;

import java.time.LocalDateTime;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;