package ru.dburyakov.web_showcase;

import com.google.inject.servlet.GuiceFilter;
import io.undertow.Undertow;
import io.undertow.servlet.Servlets;
import io.undertow.servlet.api.DeploymentInfo;
import org.jboss.resteasy.plugins.guice.GuiceResteasyBootstrapServletContextListener;
import org.jboss.resteasy.plugins.server.undertow.UndertowJaxrsServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Application;

public class Launcher {

    private static final Logger log = LoggerFactory.getLogger(Launcher.class);
    private static final String host = "localhost";
    private static final int port = 9000;

    public static void main(String[] args) {

        UndertowJaxrsServer server = new UndertowJaxrsServer();
        DeploymentInfo deploymentInfo = server
                .undertowDeployment(Application.class)
                .setDeploymentName("undertow_showcase")
                .setContextPath("/")
                .addListener(Servlets.listener(GuiceResteasyBootstrapServletContextListener.class))
                .addFilter(Servlets.filter(GuiceFilter.class))
                .addInitParameter("resteasy.guice.modules",
                        "ru.dburyakov.web_showcase.context.ProductionModule, " +
                        "org.jboss.resteasy.plugins.guice.ext.JaxrsModule"
                );

        server.deploy(deploymentInfo);

        log.info("Building server [ address: {}, port: {}] ...", host, port);
        Undertow.Builder builder = Undertow.builder().addHttpListener(port, host);
        server.start(builder);

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                log.info("stopping http server");
                server.stop();
            } catch (Exception e) {
                log.error("", e);
            }
        }));


        log.info("http server has started");

    }
}
