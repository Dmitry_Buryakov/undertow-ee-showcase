package ru.dburyakov.web_showcase.service;

import ru.dburyakov.web_showcase.http.dto.BookDTO;

import java.util.Optional;
import java.util.stream.Stream;

public interface BookService {
    /**
     * Поиск всех книг
     * @return
     */
    public Stream<BookDTO> allBooks();

    /**
     * Поиск одной книги
     * @param id идентификатор книги
     * @return
     */
    public Optional<BookDTO> findBook(Long id);
}
