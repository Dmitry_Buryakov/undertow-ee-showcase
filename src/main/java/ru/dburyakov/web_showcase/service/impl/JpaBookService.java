package ru.dburyakov.web_showcase.service.impl;

import ru.dburyakov.web_showcase.context.DataPopulator;
import ru.dburyakov.web_showcase.entity.Book;
import ru.dburyakov.web_showcase.http.dto.BookDTO;
import ru.dburyakov.web_showcase.service.BookService;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.Optional;
import java.util.stream.Stream;

public class JpaBookService implements BookService {

    @Inject
    private EntityManager em;

    @Inject
    public JpaBookService(DataPopulator dataPop) {
        dataPop.populateData();
    }

    @Override
    @Transactional
    public Stream<BookDTO> allBooks() {
        return em.createQuery("from " + Book.class.getName(), Book.class).getResultStream().map(BookDTO::of);
    }

    @Override
    @Transactional
    public Optional<BookDTO> findBook(Long id) {
        return Optional.ofNullable(em.find(Book.class, id)).map(BookDTO::of);
    }

}
